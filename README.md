#  SOME NETWORKING PROGRAMS <br />
## THESE ARE PARTS OF UNIVERSITY COURSE WORK <br />

### ASSIGNMENT 1 <br />
Setup a DOS environment for networking operation. A clone of virtual box DOS image is provided. In particular TC contains the C compiler, MTCP contains various networking tools (DHCP, FTP etc.) and NETWORK contains a sample program to emulate working of data-link layer (i.e. it can transfer and receive text messages using MAC ID).The program has been tested under FreeDos1.2 platform running on virtualbox on linux host.
### ASSIGNMENT 2 <br />
Create a chat environment with two clients and one router. If clients are in same network message is passed directly through mac id, if they are on different networks message is passed via router through ip address. The ip addresses belonging to a network are apriori stored in text files, coresponding text files are accessable to each node. The program has been tested under FreeDos1.2 platform on virtualbox on linux host.
### ASSIGNMENT 3 <br />
Create a client server model using system provided library functions utilising TCP protocol. Sqlite3 has been used to store username/password and username/socket_descriptors. To start server syntax is {sever-executable} {port}. For clients to join the server syntax is {client-executable} {server-address} {port} {username}. Syntax of message is @{targetname}:{message}. To exit program (for both server and client) command is EXIT. The program has been tested on linux platform.
### ASSIGNMENT 4 <br />
Design a tftp client for linux platform. To compile gcc client.c -o client. To run ./client {-h host} {-t timeout} {-r max_retry} {-s silent}. Available operations are get {filename}, put {filename}, timeout {value}, retry {value}, silent {value} and exit. Defaults are host="127.0.0.1", max_retry = 10, timeout = 1 sec, silent = 1. Tested with tftpd-hpa server on ubuntu 16.04.
