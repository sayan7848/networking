#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdarg.h>
#include <getopt.h>
#include <unistd.h>
#include <pthread.h>
#define FRAME_SIZE 516
#define DATA_SIZE 512
#define SIZE 256

int TIMEOUT = 1;	//in seconds
int MAX_RETRY = 10;
int silent = 1;

char** get_parsed_command(char*);
int rrq_wrq(char*, int, struct sockaddr_in, unsigned char*, char*);
int send_file(char*, int, struct sockaddr_in);
int recv_file(char*, int, struct sockaddr_in);
int send_ack(int, unsigned char*, struct sockaddr_in);

int main(int argc, char** argv)
{
	struct sockaddr_in serv_addr;
	int sock_fd, port=69;
	char *filename = "large.mp3", ch, *address = "127.0.0.1";
	struct hostent *server;

	//get cmd arguments
	while((ch = getopt(argc, argv, "h:t:r:s:")) != -1)
	{
		switch(ch)
		{
			case 'h':
				address = optarg;
				break;
			case 't':
				TIMEOUT = atoi(optarg);
				break;
			case 'r':
				MAX_RETRY = atoi(optarg);
				break;
			case 's':
				silent = atoi(optarg);
				break;
			default:
				printf("unknown option, format is <execuatbe> -h <host>\n");
				break;

		}
	}

	server = gethostbyname(address);
	//set up serv_addr
	bzero((char *)&serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
	serv_addr.sin_port = htons(port);

	//get socket sock_fd
	if((sock_fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
	{
		perror("socket failed");
		return -1;
	}

	while(1)
	{
		printf("tinytftp > ");
		char raw_command[SIZE], c;
		int i = 0;
		while((c=getchar())!='\n')
		{
			raw_command[i++] = c;
		}
		raw_command[i] = '\0';
		char** parsed_command = get_parsed_command(raw_command);
		if(strcmp(parsed_command[0], "get") == 0)
		{
			//download file from server
			printf("Status : %d\n", recv_file(parsed_command[1], sock_fd, serv_addr));
		}
		else if(strcmp(parsed_command[0], "put") == 0)
		{
			//upload file to server
			printf("Status : %d\n", send_file(parsed_command[1], sock_fd, serv_addr));
		}
		else if(strcmp(parsed_command[0], "exit") == 0)
		{
			//quit program
			break;
		}
		else if(strcmp(parsed_command[0], "timeout") == 0)
		{
			TIMEOUT = atoi(parsed_command[1]);
		}
		else if(strcmp(parsed_command[0], "retry") == 0)
		{
			MAX_RETRY = atoi(parsed_command[1]);
		}
		else if(strcmp(parsed_command[0], "silent") == 0)
		{
			silent = atoi(parsed_command[1]);
		}
		else
		{
			perror("unknown command");
		}
	}
	shutdown(sock_fd, SHUT_RDWR);
	close(sock_fd);
	return 0;
}



//parse user input
char** get_parsed_command(char* raw_command)
{
	char **parsed_command = (char**)calloc(2, sizeof(char*));
	char *token = strtok(raw_command, " ");
	int index = 0;
	while(token != NULL)
	{
		parsed_command[index++] = token;
		token = strtok(NULL, " ");
	}
	return parsed_command;
}


//send file to server
int send_file(char* filename, int sock_fd, struct sockaddr_in servaddr)
{
	unsigned char read_buffer[FRAME_SIZE] = {'\0'}, write_buffer[FRAME_SIZE] = {'\0'}, file_buffer[DATA_SIZE] = {'\0'};
	FILE *fp = fopen(filename, "rb");
	if(fp == NULL)
	{	
		perror("no such file");
		return -3;
	}
	//prepare for wrq sending
	unsigned char opcode[3];
	opcode[0] = 0x00;
	opcode[1] = 0x02;
	opcode[2] = 0x00;

	int retry = MAX_RETRY, n;
	while(retry > 0 && (n = rrq_wrq(filename, sock_fd, servaddr, opcode, "octet")) < 0)
	{
		if(!silent)
		perror("could not send wrq, retrying");
		retry--;
	}
	if(retry == 0)
	{
		perror("wrq request timeout");
		return -1;
	}
	retry = MAX_RETRY;
	while(retry > 0)
	{
		//set socket timeout
		struct timeval tv;
		tv.tv_sec = TIMEOUT;  //1 sec timeout//
		tv.tv_usec = 0;  // Not init'ing this can cause strange errors
		socklen_t socklen;
		setsockopt(sock_fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv,sizeof(struct timeval));
		n = recvfrom(sock_fd, read_buffer, FRAME_SIZE, 0, (struct sockaddr*)&servaddr, &socklen);
		if(n < 0)
		{
			if(!silent)
			perror("could not get ack from server, retrying");
			retry--;
		}
		else if(read_buffer[0]==0x00 && read_buffer[1]==0x05)		//error code
		{
			if(!silent)
			{
				perror("error message from server, retrying");
				printf("error code : %x%x 		error message : %s", read_buffer[2], read_buffer[3], read_buffer + 4);
			}
			retry--;
		}
		else
		{
			break;
		}
	}
	if(retry == 0)
	{
		perror("timeout while getting ack from server");
		return -2;
	}
	int block = 1;
	int stat = 1;
	while(stat > 0)
	{
		bzero(file_buffer, DATA_SIZE);
		int fsize = fread(file_buffer, 1, DATA_SIZE, fp);
		if(fsize < DATA_SIZE)
		{
			stat = 0;
		}
		bzero(write_buffer, FRAME_SIZE);
		write_buffer[0] = 0x00;
		write_buffer[1] = 0x03;
		char block_array[2];
		block_array[0] = (block & 0xff00) >> 8;
		block_array[1] = (block & 0x00ff);
		write_buffer[2] = block_array[0];
		write_buffer[3] = block_array[1];
		memcpy(write_buffer + 4, file_buffer, fsize);
		retry = MAX_RETRY;
		block++;
		while(retry > 0)
		{
			int subretry = MAX_RETRY;
			while(subretry > 0 && (n=sendto(sock_fd, write_buffer, fsize+4, 0, (struct sockaddr*)&servaddr, sizeof(servaddr))) < 0)
			{
				subretry--;
			}
			if(subretry == 0)
			{
				perror("retry limit exceeded, could not send data frame");
				return -4;
			}
			subretry = MAX_RETRY;
			//set socket timeout
			struct timeval tv;
			tv.tv_sec = TIMEOUT;  //1 sec timeout//
			tv.tv_usec = 0;  // Not init'ing this can cause strange errors
			socklen_t socklen;
			setsockopt(sock_fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv,sizeof(struct timeval));
			bzero(read_buffer, FRAME_SIZE);
			while(subretry > 0 && (n=recvfrom(sock_fd, read_buffer, FRAME_SIZE, 0, (struct sockaddr*)&servaddr, &socklen)) < 0)
			{
				subretry--;
			}
			if(subretry == 0)
			{
				perror("retry limit exceeded, could not receive data frame from server");
				return -5;
			}

			if(read_buffer[0] == 0x00 && read_buffer[1] == 0x05)
			{
				retry--;
				continue;
			}
			if(read_buffer[0] == 0x00 && read_buffer[1] == 0x04)
			{
				if(!silent)
				{
					printf("ack received for block %02x %02x\n", read_buffer[2], read_buffer[3]);
					printf("ack expected for block %02x %02x\n", block_array[0], block_array[1]);
					printf("\n\n");
				}
				if(read_buffer[2] == block_array[0] && read_buffer[3] == block_array[1])
				{
					break;
				}
				else
				{
					retry--;
					continue;
				}
			}
		}
		if(retry == 0)
		{
			perror("maximum retry limit crossed");
			return -6;
		}
	}
	fclose(fp);
	return 1;
}


//receive file from server
int recv_file(char *filename, int sock_fd, struct sockaddr_in servaddr)
{
	FILE* fp = fopen(filename, "wb");
	if(fp == NULL)
	{
		perror("could not create file");
		return -1;
	}

	unsigned char read_buffer[FRAME_SIZE], file_buffer[DATA_SIZE];
	unsigned char opcode[2];
	//set up opcode
	opcode[0] = 0x00;
	opcode[1] = 0x01;
	//send rrq
	int retry = MAX_RETRY, n, stat = 1;
	while(retry > 0 && (n=rrq_wrq(filename, sock_fd, servaddr, opcode, "octet")) < 0)
	{
		retry--;
	}
	if(retry == 0)
	{
		perror("rrq failed");
		return -2;
	}
	retry = MAX_RETRY;
	int block = 1;
	while(stat > 0)
	{
		//the expected block number
		unsigned char block_array[2];
		block_array[0] = (block & 0xff00) >> 8;
		block_array[1] = (block & 0x00ff);
		//set socket timeout
		struct timeval tv;
		tv.tv_sec = TIMEOUT;  //1 sec timeout//
		tv.tv_usec = 0;  // Not init'ing this can cause strange errors
		socklen_t socklen;
		setsockopt(sock_fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv,sizeof(struct timeval));
		//buffer to store incoming frame
		unsigned char read_buffer[FRAME_SIZE] = {'\0'};

		//read from server
		while(retry > 0 && (n = recvfrom(sock_fd, read_buffer, FRAME_SIZE, 0, (struct sockaddr*)&servaddr, &socklen)) < 0)
		{
			if(!silent)
			perror("error reading from socket, retrying");
			retry--;
		}
		if(retry == 0)
		{
			perror("maximum retry limit crossed");
			return -2;
		}
		if(n < FRAME_SIZE)	//case when last byte is transmitted
		{
			stat = 0;
		}

		//check if error like file not found
		if(read_buffer[0] == 0x00 && read_buffer[1] == 0x05)
		{
			perror("error message from server");
			printf("error code : %x%x, error message : %s\n", read_buffer[2], read_buffer[3], read_buffer + 4);
			return -5;
		}

		if(!silent)
		{
			printf("Block received : %02x %02x\n", read_buffer[2], read_buffer[3]);
			printf("Block expected : %02x %02x\n", block_array[0], block_array[1]);
			printf("\n\n");
		}
		//check if frame number matches with expected
		if(read_buffer[2] != block_array[0] || read_buffer[3] != block_array[1])	//does not match
		{
			if(block == 1)	//resend rrq
			{
				int sub_retry = MAX_RETRY;
				while(sub_retry > 0 && (rrq_wrq(filename, sock_fd, servaddr, opcode, "octet")) < 0)
				{
					sub_retry--;
				}
				if(sub_retry == 0)
				{
					perror("rrq failed");
					return -2;
				}
			}
			else	//for other blocks, resend previous ack
			{
				unsigned char sub_block_array[2];
				sub_block_array[0] = ((block - 1) & 0xff00) >> 8;
				sub_block_array[1] = (block - 1) & 0x00ff;
				int sub_retry = MAX_RETRY;
				while(sub_retry > 0 && (send_ack(sock_fd, sub_block_array, servaddr)) < 0)
				{
					sub_retry--;
				}
				if(sub_retry == 0)
				{
					perror("maximum retry limit crossed, could not resend ack");
					return -3;
				}
			}
			continue;
		}
		else	//frame number matches, write to file and send ack
		{
			fwrite(read_buffer + 4, 1, n-4, fp);
			//send ack
			int sub_retry = MAX_RETRY;
			while(sub_retry > 0 && send_ack(sock_fd, block_array, servaddr) < 0)
			{
				sub_retry--;
			}
			if(sub_retry == 0)
			{
				perror("maximum retry limit crossed, could not send ack");
				return -4;
			}

		}
		block++;
	}
	fclose(fp);
	return 1;
}


//performs the initial read or write request
int rrq_wrq(char* filename, int sock_fd, struct sockaddr_in servaddr, unsigned char* opcode, char* mode)
{
	char buffer[FRAME_SIZE] = {'\0'};
	snprintf(buffer, FRAME_SIZE, "%c%c%s%c%s%c", opcode[0], opcode[1], filename, 0x00, mode, 0x00);

	if(sendto(sock_fd, buffer, FRAME_SIZE, 0, (struct sockaddr*)&servaddr, sizeof(servaddr)) != FRAME_SIZE)
	{
		return -1;
	}

	return 0;
}


//send ack to server
int send_ack(int sock_fd, unsigned char *block, struct sockaddr_in servaddr)
{
	char buffer[4];
	buffer[0] = 0x00;
	buffer[1] = 0x04;
	buffer[2] = block[0];
	buffer[3] = block[1];

	if(sendto(sock_fd, buffer, 4, 0, (struct sockaddr*)&servaddr, sizeof(servaddr)) != 4)
	{
		return -1;
	}
	return 0;
}
