#include "constants.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <string.h>
#include <netdb.h>
#include <pthread.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

void *receiver(void *ptr);
int sock_fd;
int register_user(int, char *);
void end_socket();

void getPassword(char password[])
{
    static struct termios oldt, newt;
    int i = 0;
    int c;

    /*saving the old settings of STDIN_FILENO and copy settings for resetting*/
    tcgetattr( STDIN_FILENO, &oldt);
    newt = oldt;

    /*setting the approriate bit in the termios struct*/
    newt.c_lflag &= ~(ECHO);

    /*setting the new bits*/
    tcsetattr( STDIN_FILENO, TCSANOW, &newt);

    /*reading the password from the console*/
    while ((c = getchar()) != '\n' && c != EOF && i < SIZE)
    {
        password[i++] = c;
    }
    password[i] = '\0';

    /*resetting our old STDIN_FILENO*/
    tcsetattr( STDIN_FILENO, TCSANOW, &oldt);

}

int main(int argc, char **argv)
{
    int port_no, n, option = 1;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    char *name;
    pthread_t receiver_thread;

    if (argc < 4)
    {
        printf("Format is : <executable> <server> <port> <username>\n");
        exit(0);
    }

    port_no = atoi(argv[2]);
    name = (char *)calloc(SIZE, sizeof(char));
    strcpy(name, argv[3]);

    sock_fd = socket(AF_INET, SOCK_STREAM, 0);  //establish a socket
    setsockopt(sock_fd, SOL_SOCKET, SO_REUSEADDR, &option,
               sizeof(option)); //set socket as reusable

    if (sock_fd < 0)
    {
        perror("error in establishing socket");
        exit(0);
    }

    server = gethostbyname(argv[1]);    //get server info from provided arguments
    if (server == NULL)
    {
        perror("no such host");
        exit(0);
    }
    //fillup server details
    bzero((char *)&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr,
          server->h_length);
    serv_addr.sin_port = htons(port_no);

    //connect to server
    if (connect(sock_fd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        perror("error connecting");
        exit(0);
    }
    if (register_user(sock_fd, argv[3]) == -1) //sign up or login as applicable
    {
        perror(PASS_ERR);
        end_socket();
        exit(0);
    }

    if (pthread_create(&receiver_thread, NULL, receiver, (void *)&sock_fd))
    {
        perror("could not create receiving thread");
        end_socket();
        exit(0);
    }
    while (1)
    {
        //to send to another user format is @<targetname>:<message>
        char *buffer = (char *)calloc(BUFFERSIZE, sizeof(char));
        //printf("here\n");
        fgets(buffer, BUFFERSIZE, stdin);
        write(sock_fd, buffer, BUFFERSIZE);
        if (strncmp(buffer, "EXIT\n", strlen(buffer)) == 0)
        {
            printf("bye bye\n");
            break;
        }
        free(buffer);
    }
    end_socket();
    pthread_join(receiver_thread, NULL);
    return 0;
}

int register_user(int fd, char *name)
{
    int status = 1;
    char *buffer = (char *)calloc(BUFFERSIZE, sizeof(char));
    write(fd, name, strlen(name));
    if (read(fd, buffer, BUFFERSIZE) > 0)
    {
        if (strncmp(buffer, NEW_USER, strlen(NEW_USER)) == 0)
        {
            while (1)
            {
                //new user, ask for a password from user to register
                printf("%s\n", buffer);
                printf("enter new password : ");
                char *pass1 = (char *)calloc(SIZE, sizeof(char));
                getPassword(pass1);
                printf("\nre-enter the password : ");
                char *pass2 = (char *)calloc(SIZE, sizeof(char));
                getPassword(pass2);
                if (strncmp(pass1, pass2, strlen(pass2)) == 0)
                {
                    write(fd, pass1, strlen(pass1));
                    bzero(buffer, BUFFERSIZE);
                    if (read(fd, buffer, BUFFERSIZE) > 0)
                    {
                        printf("\n%s\n", buffer);
                    }
                    break;
                }
                else
                {
                    printf("passwords don't match, re-enter\n");
                }
                free(pass1);
                free(pass2);
            }
        }
        //user has registered previously, send registered password
        else if (strncmp(buffer, ALREADY_REGISTERED, strlen(ALREADY_REGISTERED)) == 0)
        {
            printf("%s\n", buffer);
            printf("enter your registered password : ");
            char *pass = (char *)calloc(SIZE, sizeof(char));
            getPassword(pass);
            write(fd, pass, strlen(pass));
            char *buff = (char *)calloc(BUFFERSIZE, sizeof(char));
            if (read(fd, buff, BUFFERSIZE) > 0)
            {
                printf("\n%s\n", buff);
                if (strncmp(buff, PASS_ERR, strlen(PASS_ERR)) == 0)
                {
                    status = -1;
                }
            }
            free(pass);
            free(buff);
        }
    }
    return status;
}

//function to get input from server
void *receiver(void *ptr)
{
    int fd = *(int *)ptr;
    int n;
    while (1)
    {
        char *buffer = (char *)calloc(BUFFERSIZE, sizeof(char));
        if (read(fd, buffer, BUFFERSIZE) > 0)
        {
            if (strncmp(buffer, DUMMY, strlen(DUMMY)) != 0)
            {
                printf("INCOMING MESSAGE : %s\n", buffer);
                if (strncmp(buffer, ALREADY_ACTIVE, strlen(ALREADY_ACTIVE)) == 0)
                {
                    end_socket();
                    exit(0);
                }
            }
        }
        else
        {
            perror("no response from server");
            end_socket();
            exit(0);
        }
    }
}

void end_socket()
{
    shutdown(sock_fd, SHUT_RDWR);
    close(sock_fd);
}
