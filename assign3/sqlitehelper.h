#include <sqlite3.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int socketNum = -1;
char* entryName = NULL;

void setDefault()
{
	socketNum = -1;
	entryName = NULL;
}

int getSocketNum()
{
	return socketNum;
}

void getName(char** result)
{
	if(entryName!=NULL)
	{
		*result = (char*)malloc(strlen(entryName));
		strcpy(*result, entryName);
	}
	else
	{
		*result = NULL;
	}
}

static int callback(void *NotUsed, int argc, char **argv, char **azColName){
   	int i;
   	for(i=0; i<argc; i++)
	{
		//printf("%s = %s\n", azColName[i], argv[i] ?  : "NULL");
		if(argv[i])
		{
			socketNum = atoi(argv[i]);
			entryName = (char*)malloc(sizeof(argv[i]));
			strcpy(entryName, argv[i]);		
		}
		else
		{
			socketNum = -999;
			entryName = NULL;
		}
   	}
   	printf("%d\n", argc);
   	return 0;
}

/*int callback(void *arg, int argc, char **argv, char **colName) {                                
    int i;
    for(i=0; i<argc; i++){
        printf("%s = %s\t", colName[i], argv[i] ?  : "NULL");
    }
    printf("%d\n", argc);
    printf("\n");
    return 0;
}*/

sqlite3* create_database(char* database_name)
{
	sqlite3* db;
	if(sqlite3_open(database_name, &db))
	{
		return NULL;
	}
	else
	{
		return db;
	}
}

int close_database(sqlite3* db)
{
	if(sqlite3_close(db) == SQLITE_OK)
	{
		return SQLITE_OK;
	}
	else
	{
		return -1;
	}
}

int exec_cmd(sqlite3* db, char* cmd)
{
	setDefault();
	char* zerrMsg = 0;
	if(sqlite3_exec(db, cmd, callback, NULL, &zerrMsg) != SQLITE_OK)
	{
		sqlite3_free(zerrMsg);
		return -1;
	}
	else
	{
		return 	SQLITE_OK;
	}
}

/*int main()
{
	char* db_name = "test.db";
	sqlite3* db = create_database(db_name);

	char* create_table = "CREATE TABLE IF NOT EXISTS USERS ("\
			      "NAME TEXT PRIMARY KEY NOT NULL,"\
			      "SOCKET INTEGER NOT NULL);";
	char* insert_row = "INSERT INTO USERS (NAME, SOCKET) "\
			     "VALUES ('NILANJANA58', 21), "\
			     "('ARUNKR', 22);";
	char* select = "SELECT SOCKET FROM USERS WHERE NAME = 'ARUNKR'";
	char* remove_table = "DROP TABLE USERS";
	if(exec_cmd(db, create_table) != -1)
	{
		printf("table created\n");
	}
	else
	{
		printf("table creation failed\n");
	}

	if(exec_cmd(db, insert_row) != -1)
	{
		printf("values inserted\n");
	}
	else
	{
		printf("error in inserting rows\n");
	}
	if(exec_cmd(db, select) != -1)
	{
		printf("select completed successfully\n");
	}
	else
	{
		printf("could not perform select\n");
	}
	if(exec_cmd(db, remove_table) != -1)
	{
		printf("removed table\n");
	}
	else
	{
		printf("could not remove database\n");
	}
	if(close_database(db) != -1)
	{
		printf("closed database\n");
	}
	else
	{
		printf("could not close database\n");
	}
	return 0;
}*/
