#include "sqlitehelper.h"
#include "constants.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <string.h>

void *handler(void *);  //thread to handle each new connection
void *server_terminal(void *);  //thread to read from server terminal
void exit_func();   //exit from program
void clean_sock();  //clean up and shut down sockets
void int_handler(int);  //handle ctrl-c
int check_login(char *name, int);  //check if user is already registered
void handle_new_user(char *name, int);  //handle incoming user
int check_existing(char *name, int);    //check if user is already active
void continue_communication(char *,
                            int);     //continue normal message transfer
char *int2str(int);     //integer to string
void logout(char *, int);

struct container
{
    int sock_fd;
    char *name; //stores node name or the message to send
};
int sock_fd, temp_sock_fd;
sqlite3 *db;

struct container parse_input_message(char *,
                                     char *); //parse input message to find dest sock_fd and the actual message to send

int main(int argc, char **argv)
{
    signal(SIGINT, int_handler);    //install the signal handler
    if (argc < 2)
    {
        printf("format is <executable> <port_no>\n");   //the input format
        exit(0);
    }
    int port_no, cli_len, option = 1;
    struct sockaddr_in serv_addr, cli_addr;
    char *db_name = "USERS.DB";
    char *create_table_socket =
        "CREATE TABLE IF NOT EXISTS USERS (NAME TEXT PRIMARY KEY NOT NULL, SOCKET INTEGER NOT NULL);";
    char *create_table_pass =
        "CREATE TABLE IF NOT EXISTS PASS (NAME TEXT PRIMARY KEY NOT NULL, PASSWORD TEXT NOT NULL);";
    pthread_t handler_thread, terminal_thread; //threads used

    db = create_database(
             db_name); //create the database / get a pointer to existing one
    if (exec_cmd(db, create_table_socket) == -1
            || exec_cmd(db,
                        create_table_pass))    //create the users table, exit program on failure
    {
        perror("could not create tables");
        exit(0);
    }
    sock_fd = socket(AF_INET, SOCK_STREAM, 0);  //create a socket
    if (sock_fd < 0) //check for error
    {
        perror("error in creating socket");
        exit(0);
    }
    setsockopt(sock_fd, SOL_SOCKET, SO_REUSEADDR, &option,
               sizeof(option)); //set socket as reusable
    pthread_create(&terminal_thread, NULL, server_terminal,
                   (void *)&sock_fd); //spawn a new thread to handle trerminal input in server
    bzero((char *)&serv_addr,
          sizeof(serv_addr));   //initialize server address to bzero
    port_no = atoi(argv[1]);            //get port num from cmd arguments
    serv_addr.sin_family = AF_INET;         //fiil
    serv_addr.sin_addr.s_addr = INADDR_ANY; //up
    serv_addr.sin_port = htons(port_no);    //server info
    //bind to the socket descriptor
    if (bind(sock_fd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        perror("error in binding");
        exit(0);
    }
    printf("bind done\n");
    listen(sock_fd, 10);    //set up a listener for the given port
    printf("waiting for new connection\n");
    cli_len = sizeof(struct sockaddr_in);

    while ((temp_sock_fd = accept(sock_fd, (struct sockaddr *)&cli_addr,
                                  (socklen_t *)&cli_len)) >= 0)
    {
        printf("new connection accepted\n");
        char *name = (char *)calloc(SIZE, sizeof(char));
        if (read(temp_sock_fd, name, SIZE) > 0)
        {
            //fill up container with sock_fd and name
            struct container cont;
            cont.name = (char *)calloc(SIZE, sizeof(char));
            strcpy(cont.name, name);
            cont.sock_fd = temp_sock_fd;
            //spawn a thread to handle the new connection
            pthread_create(&handler_thread, NULL, handler, (void *)&cont);
        }
        free(name);
        name = NULL;
    }
    return 0;
}

void *handler(void *ptr)
{
    struct container cont = *(struct container *)ptr;
    handle_new_user(cont.name, cont.sock_fd);
    if (check_existing(cont.name, cont.sock_fd) != -1)
    {
        write(cont.sock_fd, DUMMY, strlen(DUMMY));
        continue_communication(cont.name, cont.sock_fd);
    }
    else
    {
        write(cont.sock_fd, ALREADY_ACTIVE, strlen(ALREADY_ACTIVE));
        pthread_exit(0);
    }
    return NULL;
}

void handle_new_user(char *name, int fd)
{
    char *temp;
    char *buff = (char *)calloc(BUFFERSIZE, sizeof(char));
    char *cmd = "SELECT PASSWORD FROM PASS WHERE NAME = '";
    strcat(buff, cmd);
    strcat(buff, name);
    strcat(buff, "';");
    exec_cmd(db, buff);
    getName(&temp); //check if user is already registered
    if (temp == NULL)
    {
        write(fd, NEW_USER, strlen(
                  NEW_USER));  //user not registered, tell client to send new password
        bzero(buff, BUFFERSIZE);
        char *temp_buff = (char *)calloc(SIZE, sizeof(char));
        if (read(fd, temp_buff, SIZE) > 0)
        {
            char *cmd = "INSERT INTO PASS (NAME, PASSWORD) VALUES ('";
            strcat(buff, cmd);
            strcat(buff, name);
            strcat(buff, "', '");
            strcat(buff, temp_buff);
            strcat(buff, "');");
            int n = exec_cmd(db, buff);
            // printf("%s : %d\n", buff, n);
            write(fd, USER_REGISTERED,
                  strlen(USER_REGISTERED));    //send confirmation to client
        }
    }
    else
    {
        write(fd, ALREADY_REGISTERED,
              strlen(ALREADY_REGISTERED));  //user registered, send message to client to send registered password
        char *temp_buff = (char *)calloc(SIZE, sizeof(char));
        if (read(fd, temp_buff, SIZE) > 0)
        {
            if (strncmp(temp_buff, temp, strlen(temp)) == 0)
            {
                write(fd, PASS_MATCH, strlen(PASS_MATCH));  //password matches, tell client
            }
            else
            {
                write(fd, PASS_ERR, strlen(PASS_ERR));  //password dont matches, tell client
                pthread_exit(0);
            }
        }
    }
}

int check_existing(char *name, int fd)
{
    char *buff = (char *)calloc(BUFFERSIZE, sizeof(char));
    char *cmd = "SELECT SOCKET FROM USERS WHERE NAME = '";
    strcat(buff, cmd);
    strcat(buff, name);
    strcat(buff, "';");
    exec_cmd(db, buff);     //check if user is already active
    int status = getSocketNum();
    // printf("%s : %d\n", buff, status);
    if (status != -1)
    {
        return -1;
    }
    else
    {
        bzero(buff, BUFFERSIZE);
        char *cmd = "INSERT INTO USERS (NAME, SOCKET) VALUES ('";
        strcat(buff, cmd);
        strcat(buff, name);
        strcat(buff, "', ");
        strcat(buff, int2str(fd));
        strcat(buff, ");");
        // printf("%s\n", buff);
        return exec_cmd(db,
                        buff);  //user was not previously active, store sockfd in db
    }
}

void *server_terminal(void *ptr)
{
    int sock_fd = *(int *)ptr;
    while (1)
    {
        char *buffer = (char *)calloc(BUFFERSIZE, sizeof(char));
        fgets(buffer, BUFFERSIZE, stdin);
        if (strncmp(buffer, EXIT, strlen(buffer)) == 0)
        {
            exit_func();
        }
        free(buffer);
    }
    return NULL;
}

void exit_func()
{
    exec_cmd(db, "DROP TABLE USERS");
    close_database(db);
    shutdown(sock_fd, SHUT_RDWR);
    shutdown(temp_sock_fd, SHUT_RDWR);
    close(sock_fd);
    close(temp_sock_fd);
    exit(0);
}

void int_handler(int sig_num)
{
    exit_func();
}

char *int2str(int item)
{
    char *buff = (char *)calloc(SIZE, sizeof(char));
    snprintf(buff, SIZE, "%d", item);
    return buff;
}

void continue_communication(char *sender, int fd)
{
    while (1)
    {
        char *buffer;
        buffer = (char *)calloc(BUFFERSIZE, sizeof(char));
        if (read(fd, buffer, BUFFERSIZE) > 0)
        {
            //  printf("%s\n", buffer);
            if (strncmp(buffer, EXIT, strlen(EXIT)) == 0)
            {
                logout(sender, fd);
                pthread_exit(0);
            }
            struct container cont = parse_input_message(sender, buffer);
            if (cont.sock_fd == -1)
            {
                write(fd, WRONG_FORMAT, strlen(WRONG_FORMAT));
            }
            else
            {
                write(cont.sock_fd, cont.name, strlen(cont.name));
            }
        }
        free(buffer);
    }
}

void logout(char *name, int fd)
{
    char *buff = (char *)calloc(BUFFERSIZE, sizeof(char));
    sprintf(buff, "DELETE FROM USERS WHERE SOCKET = %d;", fd);
    // printf("%s\n", buff);
    exec_cmd(db, buff);
    // printf("user logged out\n");
    printf("user %s logged out\n", name);
    free(buff);
}

struct container parse_input_message(char *sender, char *message)
{
    char *msg = (char *)calloc(BUFFERSIZE, sizeof(char));
    char *cmd = (char *)calloc(BUFFERSIZE, sizeof(char));
    char *name = (char *)calloc(SIZE, sizeof(char));
    struct container cont;

    int i = 0, j;
    for (i = 0; i < strlen(message) - 1
            && message[i + 1] != ':'; i++) //':' is delimeter
    {
        name[i] = message[i + 1];
    }
    for (j = i + 2; j < strlen(message); j++)
    {
        msg[j - (i + 2)] = message[j];
    }
    msg[j - (i + 2)] = '\0';
    name[i] = '\0';
    sprintf(cmd, "SELECT SOCKET FROM USERS WHERE NAME = '%s';", name);
    strcat(msg, " << ");
    strcat(msg, sender);
    exec_cmd(db, cmd);
    cont.sock_fd = getSocketNum();
    cont.name = (char *)calloc(BUFFERSIZE, sizeof(char));
    strcpy(cont.name, msg);
    return cont;
};
